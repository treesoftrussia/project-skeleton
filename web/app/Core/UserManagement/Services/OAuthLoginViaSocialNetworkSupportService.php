<?php

namespace App\Core\UserManagement\Services;

use App\Core\UserManagement\Entity\Support\WebLoginEntity;
use App\Core\UserManagement\Interfaces\UserRepositoryInterface;
use App\Core\UserManagement\Services\SocialNetworkProviders\WechatSocialNetworkProvider;
use App\DAL\Repository\UserRepository;
use League\OAuth2\Server\Exception\InvalidCredentialsException;
use Mildberry\Kangaroo\Libraries\Container\ContainerInterface;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;

class OAuthLoginViaSocialNetworkSupportService extends AbstractService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * OAuthLoginViaSocialNetworkSupportService constructor.
     *
     * @param ContainerInterface      $container
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(ContainerInterface $container, UserRepositoryInterface $userRepository)
    {
        parent::__construct($container);
        $this->userRepository = $userRepository;
    }

    /**
     * @param $code
     * @param $type
     *
     * @return mixed
     *
     * @throws InvalidCredentialsException
     */
    public function getToken($code, $type)
    {
        $provider = $this->getProvider($type);

        $socialNetworkProfile = $provider->requestAccess($code);
        $user = $provider->getUser();

        $user = $this->userRepository->createOrUpdateIfExistsSocialNetworkEntry($user, $socialNetworkProfile, (new UserOptions())->setWithSocialNetworkEntry(true));

        return (new OauthSupportService())->getTokenByPasswordGrantType((new WebLoginEntity())->setLogin($user->getUserName())->setPassword($user->getPassword()));
    }

    private function getProvider($type)
    {
        switch ($type) {
            case 'wechat':
                return $this->container->get(WechatSocialNetworkProvider::class);
            default:
                throw new RuntimeException('Specified login type not supported.');
        }
    }
}
