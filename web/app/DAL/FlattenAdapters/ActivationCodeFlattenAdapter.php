<?php

namespace App\DAL\FlattenAdapters;

use App\Core\UserManagement\Entity\Support\ActivationCodeEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class ActivationCodeFlattenAdapter.
 */
class ActivationCodeFlattenAdapter extends AbstractAdapter
{
    /**
     * @param ActivationCodeEntity $activationCodeEntity
     *
     * @return array
     */
    public function transform($activationCodeEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'phone' => $sanitizer($activationCodeEntity->getPhone(), 'string'),
            'activation_code' => $sanitizer($activationCodeEntity->getActivationCode(), 'string'),
            'expires_at' => $sanitizer($activationCodeEntity->getExpiresAt(), 'integer'),
            'refresh_at' => $sanitizer($activationCodeEntity->getRefreshAt(), 'integer'),
        ], [null]);
    }
}
