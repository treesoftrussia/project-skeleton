<?php

namespace App\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

class RoleModel extends Elegant
{
    protected $table = 'roles';

    public function users()
    {
        return $this->belongsToMany(UserModel::class, 'user_roles', 'role_id', 'user_id');
    }

    public function scopes()
    {
        return $this->belongsToMany(ScopeModel::class, 'role_oauth_scopes', 'role_id', 'oauth_scope_id');
    }
}
