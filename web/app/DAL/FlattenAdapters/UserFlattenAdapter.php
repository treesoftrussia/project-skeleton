<?php

namespace App\DAL\FlattenAdapters;

use App\Core\UserManagement\Entity\Users\UserEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class UserFlattenAdapter.
 */
class UserFlattenAdapter extends AbstractAdapter
{
    /**
     * @param UserEntity $user
     *
     * @return array
     */
    public function transform($user = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'email' => $sanitizer($user->getEmail(), 'string'),
            'username' => $sanitizer($user->getUsername(), 'string'),
            'phone' => $sanitizer($user->getPhone(), 'string'),
            'password' => $sanitizer($user->getPasswordHash(), 'string'),
        ], [null]);
    }
}
