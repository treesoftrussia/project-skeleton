<?php

namespace App\Core\UserManagement\Entity\Users;

use App\Core\UserManagement\Interfaces\ScopableInterface;
use Mildberry\Kangaroo\Libraries\Support\Behaviours\TimesTrait;

abstract class BaseUserEntity implements ScopableInterface
{
    use TimesTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
