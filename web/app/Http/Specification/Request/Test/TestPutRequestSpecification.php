<?php
namespace App\Http\Specification\Request\Test;

use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class TestPutRequestSpecification extends ASpecification
{
    protected $specificationType = ASpecification::REQUEST_SPECIFICATION;

    public function body()
    {
        return new ObjectType([
            'name' => new StringType(),
            'description' => new StringType()
        ]);
    }

}