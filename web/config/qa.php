<?php
/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
return [
    'endpoints' => [
        'suitesNamespace' => 'App\Tests\Endpoints\Suites',
        'dbName' => env('DB_TEST_DATABASE', 'forge')
    ]
];
