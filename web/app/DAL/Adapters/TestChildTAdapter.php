<?php

namespace App\DAL\Adapters;

use App\DAL\Model\TestChildTModel;
use App\Core\Test\Entity\TestChildTEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildTAdapter extends AbstractAdapter
{
    /**
     * @param TestChildTModel $model
     *
     * @return TestChildTEntity
     */
    public function transform($model = null)
    {
        $entity = new TestChildTEntity();

        $entity
            ->setId($model->id)
            ->setTestId($model->test_id)
            ->setTitle($model->title)
        ;

        return $entity;
    }
}
