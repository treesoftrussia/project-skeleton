<?php

namespace App\Http\Controller\API;

use App\Core\UserManagement\Services\OAuthLoginViaSocialNetworkSupportService;
use App\Http\Controller\Controller;
use App\Http\Requests\SocialNetwork\GetWechatAccessTokenRequest;
use App\Http\Transformers\User\TokenEntityTransformer;
use Mildberry\Kangaroo\Libraries\Resource\Resource\Resource;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class SocialNetworkController extends Controller
{
    /**
     * @param GetWechatAccessTokenRequest              $request
     * @param OAuthLoginViaSocialNetworkSupportService $service
     *
     * @return mixed
     */
    public function getAccessTokenByCode(GetWechatAccessTokenRequest $request, OAuthLoginViaSocialNetworkSupportService $service)
    {
        $tokenEntity = $service->getToken($request->get('code'), $request->get('type'));

        return Resource::make($tokenEntity, new TokenEntityTransformer());
    }
}
