<?php

namespace App\Core\UserManagement\Interfaces;

use App\Core\UserManagement\Entity\Users\UserEntity;

interface UserRepositoryInterface
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function getUser($id);

    /**
     * @param $email
     *
     * @return mixed
     */
    public function getUserByCredentials($email);

    /**
     * @param UserEntity $object
     *
     * @return mixed
     */
    public function create(UserEntity $object);

    /**
     * @param $id
     * @param UserEntity $object
     *
     * @return mixed
     */
    public function update($id, UserEntity $object);
}
