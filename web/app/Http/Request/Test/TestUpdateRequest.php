<?php

namespace App\Http\Requests\Test;

use App\Core\Test\Entity\TestTEntity;
use App\Http\Specification\Request\Test\TestPutRequestSpecification;
use Mildberry\Kangaroo\Libraries\Requests\APIRequest;
use Mildberry\Kangaroo\Libraries\RequestsValidator\RuleBuilders\TestRuleBuilder;


/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestUpdateRequest extends APIRequest
{
    protected $specificationClassName = TestPutRequestSpecification::class;

    public function createEntity()
    {
        return $this->extract($this->json()->all(), new TestTEntity());
    }
}
