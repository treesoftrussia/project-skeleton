<?php

namespace App\Http\Requests\Test;

use App\Core\Test\Entity\TestChildTEntity;
use App\Core\Test\Entity\TestTEntity;
use App\Http\Specification\Request\Test\TestPostRequestSpecification;
use App\Http\Specification\Request\TestRequestSpecification;
use Mildberry\Kangaroo\Libraries\Requests\APIRequest;
use Mildberry\Kangaroo\Libraries\RequestsValidator\RuleBuilders\TestRuleBuilder;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestStoreRequest extends APIRequest
{
    protected $specificationClassName = TestPostRequestSpecification::class;

    public function createEntity()
    {
        return $this->extract($this->json()->all(), new TestTEntity(), [
            'hint' => ['children' => 'collection:'.TestChildTEntity::class],
        ]);
    }
}
