<?php

namespace App\DAL\Model;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Mildberry\Kangaroo\Libraries\Elegant\Elegant;
use Illuminate\Auth\Authenticatable;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class UserModel extends Elegant implements AuthenticatableContract
{
    use Authenticatable;

    protected $table = 'users';
    protected $primaryKey = 'id';

    public function roles()
    {
        return $this->belongsToMany(RoleModel::class, 'user_roles', 'user_id', 'role_id');
    }
}
