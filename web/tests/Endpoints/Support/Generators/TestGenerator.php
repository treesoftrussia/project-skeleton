<?php

namespace App\Tests\Endpoints\Support\Generators;

use App\DAL\Model\TestTModel;
use Mildberry\Kangaroo\QA\Endpoints\Entity\AbstractGenerator;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestGenerator extends AbstractGenerator
{
    /**
     * @param int $times
     *
     * @return TestTModel[]
     */
    public function generate($times = 1)
    {
        return $this->entity->make(TestTModel::class, $times, ['name' => $this->faker->name, 'description' => $this->faker->text]);
    }
}
