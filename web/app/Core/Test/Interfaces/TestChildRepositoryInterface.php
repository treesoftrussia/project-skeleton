<?php

namespace App\Core\Test\Interfaces;

use App\Core\Test\Entity\TestChildTEntity;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface TestChildRepositoryInterface
{
    /**
     * @param TestChildTEntity $entity
     *
     * @return TestChildTEntity|null
     */
    public function create(TestChildTEntity $entity);
}
