<?php

namespace App\DAL\FlattenAdapters;

use App\Core\UserManagement\Entity\Users\SocialNetworkProfileEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class UserSocialNetworkFlattenAdapter.
 */
class UserSocialNetworkFlattenAdapter extends AbstractAdapter
{
    /**
     * @param SocialNetworkProfileEntity $profile
     *
     * @return array
     */
    public function transform($profile = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'internal_user_id' => $sanitizer($profile->getInternalId(), 'string'),
            'type' => $sanitizer($profile->getType(), 'string'),
            'user_id' => $sanitizer($profile->getUserId(), 'integer'),
        ], [null]);
    }
}
