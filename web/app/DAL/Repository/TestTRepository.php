<?php

namespace App\DAL\Repository;

use Illuminate\Support\Collection;
use App\Core\Test\Interfaces\TestRepositoryInterface;
use App\DAL\Model\TestTModel;
use App\DAL\Adapters\TestTAdapter;
use App\Core\Test\Entity\TestTEntity as TestEntity;
use App\DAL\FlattenAdapters\TestTFlattenAdapter;
use App\Core\Test\Interfaces\TestChildRepositoryInterface;
use Mildberry\Kangaroo\Libraries\Container\Container;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestRepository implements TestRepositoryInterface
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param $id
     *
     * @return \App\Core\Test\Entity\TestTEntity;
     */
    public function get($id)
    {
        $testEntry = TestTModel::find($id);

        if ($testEntry === null) {
            return;
        }

        /*
         * @var \App\Core\Test\Entity\Test
         */
        return (new TestTAdapter())->transform($testEntry);
    }

    /**
     * @param TestEntity $test
     *
     * @return TestEntity
     */
    public function create(TestEntity $test)
    {
        $model = TestTModel::create((new TestTFlattenAdapter())->transform($test));

        $testChildRepository = $this->container->get(TestChildRepositoryInterface::class);

        $testEntity = (new TestTAdapter())->transform($model);

        $children = new Collection();

        foreach ($test->getChildren() as $child) {
            $childEntity = $testChildRepository->create($child);
            $childEntity->setTestId($testEntity->getId());

            $children->push($childEntity);
        }

        $testEntity->setChildren($children);

        return $testEntity;
    }

    /**
     * @param int        $id
     * @param TestEntity $test
     */
    public function update($id, TestEntity $test)
    {
        return TestTModel::refresh($id, (new TestTFlattenAdapter())->transform($test));
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        TestTModel::whereId($id)->delete();

        return;
    }
    /**
     * @param $id
     *
     * @return bool
     */
    public function exists($id)
    {
        return TestTModel::whereId($id)->exists();
    }
}
