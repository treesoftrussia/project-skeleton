<?php

namespace App\Core\UserManagement\Entity\Support;

class WebLogoutResponseEntity
{
    /**
     * @var
     */
    private $redirectUrl;

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     *
     * @return WebLoginResponseEntity
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }
}
