<?php

namespace App\DAL\FlattenAdapters;

use App\Core\Test\Entity\TestTEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestTFlattenAdapter extends AbstractAdapter
{
    /**
     * @param TestTEntity $test
     *
     * @return array
     */
    public function transform($test = null)
    {
        $santinizer = $this->sanitizer()->make();

        return skip_empty([
            'name' => $santinizer($test->getName(), 'string'),
            'description' => $santinizer($test->getDescription(), 'string'),
        ], [null]);
    }
}
