<?php

namespace App\DAL\Adapters;

use App\DAL\Model\ScopeModel;
use App\Core\UserManagement\Entity\Rights\ScopeEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class ScopeAdapter.
 */
class ScopeAdapter extends AbstractAdapter
{
    /**
     * @param ScopeModel $model
     *
     * @return ScopeEntity
     */
    public function transform($model = null){
        $entity = new ScopeEntity();

        $entity
            ->setId($model->id)
            ->setDescription($model->description)
        ;

        return $entity;
    }
}
