<?php

namespace App\Core\UserManagement\Entity\Users;

use App\Core\UserManagement\Traits\ScopableUserTrait;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Cast\Cast;
use Mildberry\Kangaroo\Libraries\Support\Entity\ImageEntity;

class UserEntity extends BaseUserEntity
{
    use ScopableUserTrait;

    /**
     * @var
     */
    private $email;

    /**
     * @var
     */
    private $internalUsername;

    /**
     * @var
     */
    private $userName;

    /**
     * @var
     */
    private $phone;

    /**
     * @var
     */
    private $roles;

    /**
     * @var
     */
    private $passwordHash;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $sex;

    /**
     * @var
     */
    private $avatar;

    /**
     * @var
     */
    private $isActive;

    /**
     * @var
     */
    private $socialNetworkProfiles;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     *
     * @return UserEntity
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return UserEntity
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     *
     * @return UserEntity
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     *
     * @return UserEntity
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return UserEntity
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     *
     * @return UserEntity
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternalUsername()
    {
        return $this->internalUsername;
    }

    /**
     * @param mixed $internalUsername
     *
     * @return UserEntity
     */
    public function setInternalUsername($internalUsername)
    {
        $this->internalUsername = $internalUsername;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param $sex
     *
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = Cast::int($sex);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param $avatar
     *
     * @return $this
     */
    public function setAvatar(ImageEntity $avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSocialNetworkProfiles()
    {
        return $this->socialNetworkProfiles;
    }

    /**
     * @param Collection $socialNetworkProfiles
     *
     * @return $this
     */
    public function setSocialNetworkProfiles(Collection $socialNetworkProfiles)
    {
        $this->socialNetworkProfiles = $socialNetworkProfiles;

        return $this;
    }
}
