#!/usr/bin/env bash

find ./ -type d -exec chmod 755 {} +
find ./ -type f -exec chmod 644 {} +
chmod -R ug+rwx ./storage
chmod 777 ./deploy.sh
chown -R laradock:laradock ./