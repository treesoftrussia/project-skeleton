<?php
namespace App\Tests\Endpoints\Support\Faker\Providers;

use Faker\Provider\Base;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class Tax extends Base
{
    /**
     * @return string
     */
    public function federalTaxId()
    {
        return $this->generator->randomNumber(4).'-'.$this->generator->randomNumber(7);
    }
}
