<?php

namespace App\Http\ExceptionMiddleware;

use Closure;
use Mildberry\Kangaroo\Libraries\PageBuilder\PageData;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\RequestExceptionData;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Core
{
    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @param RequestExceptionData $data
     * @param Closure $next
     *
     * @return \Illuminate\Http\Response
     */
    public function handle($data, Closure $next)
    {
        if ($data->getException() instanceof HttpException) {
            switch ($data->getException()->getStatusCode()) {
                case 404:
                    $code = Error::ROUTE_NOT_FOUND;

                    if (!$data->getRequest()->isJson()) {
                        $pageData = new PageData();

                        return $this->error->write(view('admin.support.404', ['pageData' => $pageData]), $code, $data->getException()->getStatusCode());
                    }

                    return $this->error->write('Requested resource not found.', $code, $data->getException()->getStatusCode());
                case 405:
                    $code = Error::METHOD_NOT_ALLOWED;

                    return $this->error->write('Method not allowed.', $code, $data->getException()->getStatusCode());
            }
        }

        return $next($data);
    }
}
