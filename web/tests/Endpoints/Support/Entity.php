<?php

namespace App\Tests\Endpoints\Support;

use Mildberry\Kangaroo\QA\Endpoints\Entity\AbstractEntity;
use App\Tests\Endpoints\Support\Generators\TestGenerator;

/**
 * Class Entity.
 */
class Entity extends AbstractEntity
{
    /**
     * @return array
     */
    protected function generators()
    {
        return [
            'testEntries' => TestGenerator::class
        ];
    }
}
