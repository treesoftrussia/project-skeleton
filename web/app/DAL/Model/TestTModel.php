<?php

namespace App\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestTModel extends Elegant
{
    protected $table = 'tests_t';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function children(){
        return $this->hasMany(TestChildTModel::class, 'test_id', 'id');
    }
}
