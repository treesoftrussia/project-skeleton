<?php

namespace App\Http\Controller\API\Test;

use App\Core\UserManagement\Services\UserService;
use App\Core\Test\Services\TestService;
use App\Http\Controller\Controller;
use App\Http\Requests\Test\TestDestroyRequest;
use App\Http\Requests\Test\TestGetRequest;
use App\Http\Requests\Test\TestPatchRequest;
use App\Http\Requests\Test\TestUpdateRequest;
use App\Http\Requests\Test\TestStoreRequest;
use App\Http\Specification\Response\Test\TestResponseSpecification;
use Mildberry\Kangaroo\Libraries\Resource\Resource\Resource;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class OAuthTestController extends Controller
{
    /**
     * @param TestGetRequest $request
     * @param TestService    $testService
     * @param UserService    $userService
     *
     * @return mixed
     */
    public function index(TestGetRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Get')) {
            return Resource::make($testService->get($request->route('id')), TestResponseSpecification::class);
        }

        return null;
    }

    /**
     * @param TestStoreRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function store(TestStoreRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Store')) {
            $test = $testService->create($request->getEntity());

            return Resource::make($test, TestResponseSpecification::class, false);
        }

        return null;
    }

    /**
     * @param TestUpdateRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function update(TestUpdateRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Update')) {
            $test = $testService->update($request->route('id'), $request->getEntity());

            return Resource::make($test, TestResponseSpecification::class);
        }

        return null;
    }

    /**
     * @param TestPatchRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function patch(TestPatchRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Update')) {
            $test = $testService->update($request->route('id'), $request->getEntity());

            return Resource::make($test, TestResponseSpecification::class);
        }

        return null;
    }

    /**
     * @param TestDestroyRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function delete(TestDestroyRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Delete')) {
            $testService->delete($request->route('id'));

            return Resource::blank();
        }

        return null;
    }
}
