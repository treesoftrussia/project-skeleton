<?php namespace App\Tests\Endpoints\Support\Asserts\Test;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestEntityConstraint extends EntityConstraint
{

    /**
     * @param array $other
     * @return bool
     */
    protected function matches($other)
    {

        return
            $this->checkValue($this->data, 'name', $other['name'] , $this->errors) ||
            $this->checkValue($this->data, 'description', $other['description'] , $this->errors);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        // TODO: Implement toString() method.
    }
}