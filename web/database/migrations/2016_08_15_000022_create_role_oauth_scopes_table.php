<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleOauthScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_oauth_scopes', function (Blueprint $table) {
            $table->unsignedInteger('role_id');
            $table->string('oauth_scope_id', 40);

            $table->foreign('oauth_scope_id')->references('id')->on('oauth_scopes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_oauth_scopes', function (Blueprint $table) {
            $table->dropForeign(['oauth_scope_id']);
        });

        Schema::drop('role_oauth_scopes');
    }
}