<?php

namespace App\DAL\Repository;

use App\Core\Test\Interfaces\TestChildRepositoryInterface;
use App\DAL\Adapters\TestChildTAdapter;
use App\Core\Test\Entity\TestChildTEntity as TestChildEntity;
use App\DAL\FlattenAdapters\TestChildFlattenAdapter;
use App\DAL\Model\TestChildTModel;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildRepository implements TestChildRepositoryInterface
{
    /**
     * @param \App\Core\Test\Entity\TestChildTEntity $testChild
     *
     * @return \App\Core\Test\Entity\TestChildTEntity $testChild
     */
    public function create(TestChildEntity $testChild)
    {
        $model = TestChildTModel::create((new TestChildFlattenAdapter())->transform($testChild));

        /*
         * @var \App\Core\Test\Entity\TestChild
         */
        return (new TestChildTAdapter())->transform($model);
    }
}
