<?php
namespace App\Http\Specification\Request\Test;

use Mildberry\Kangaroo\Libraries\Specification\ASpecification;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class TestGetRequestSpecification extends ASpecification
{
    protected $specificationType = ASpecification::REQUEST_SPECIFICATION;
}