<?php

namespace App\Http\Requests\SocialNetwork;

use App\Http\Requests\APIRequest;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class GetWechatAccessTokenRequest extends APIRequest
{
    protected $pagination = false;
    protected $routeValidation = true;
}
