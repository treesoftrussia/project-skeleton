<?php

namespace App\Core\UserManagement\Services\SocialNetworkProviders;

interface SocialNetworkProviderInterface
{
    public function requestAccess($code);

    public function getUser();
}
