<?php

namespace App\Http\Requests\Test;

use App\Core\Test\Entity\TestTEntity;
use App\Http\Specification\Request\Test\TestPatchRequestSpecification;
use Mildberry\Kangaroo\Libraries\Requests\APIRequest;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestPatchRequest extends APIRequest
{
    protected $specificationClassName = TestPatchRequestSpecification::class;

    public function createEntity()
    {
        return $this->extract($this->json()->all(), new TestTEntity());
    }
}
