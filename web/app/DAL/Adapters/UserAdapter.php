<?php

namespace App\DAL\Adapters;

use App\Core\UserManagement\Entity\Users\UserEntity;
use App\DAL\Model\UserModel;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class UserAdapter.
 */
class UserAdapter extends AbstractAdapter
{

    /**
     * @param UserModel $model
     *
     * @return UserEntity
     */
    public function transform($model = null)
    {
        $entity = new UserEntity();

        $entity
            ->setId($model->id)
            ->setEmail($model->email)
            ->setUserName($model->username)
            ->setPhone($model->phone)
            ->setPasswordHash($model->password)
            ->setRoles((new RoleAdapter())->transformCollection($model->roles))
            ->setCreatedAt($model->created_at)
            ->setUpdatedAt($model->updated_at)
        ;

        return $entity;
    }
}
