<?php

namespace App\DAL\Adapters;

use App\Core\UserManagement\Entity\Rights\RoleEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class RoleAdapter.
 */
class RoleAdapter extends AbstractAdapter
{
    /**
     * @param null $model
     *
     * @return RoleEntity
     */
    public function transform($model = null){
        $entity = new RoleEntity();

        $entity
            ->setId($model->id)
            ->setMachineName($model->machine_name)
            ->setScopes((new ScopeAdapter())->transformCollection($model->scopes))
        ;

        return $entity;
    }
}
