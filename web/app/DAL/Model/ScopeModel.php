<?php

namespace App\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

class ScopeModel extends Elegant
{
    protected $table = 'oauth_scopes';

    public $incrementing = false;

    public function roles()
    {
        return $this->belongsToMany(RoleModel::class, 'role_oauth_scopes', 'oauth_scope_id', 'role_id');
    }

    public function oauthClients()
    {
        return $this->belongsToMany(OauthClientModel::class, 'oauth_client_scopes', 'scope_id', 'client_id');
    }
}
