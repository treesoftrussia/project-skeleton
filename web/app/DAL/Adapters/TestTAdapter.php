<?php

namespace App\DAL\Adapters;

use App\DAL\Model\TestTModel;
use App\Core\Test\Entity\TestTEntity as TestEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestTAdapter extends AbstractAdapter
{
    /**
     * @param TestTModel $model
     *
     * @return TestEntity
     */
    public function transform($model = null)
    {
        $entity = new TestEntity();

        $entity
            ->setId($model->id)
            ->setName($model->name)
            ->setDescription($model->description)
            ->setChildren((new TestTAdapter())->transformCollection($model->children)
            );

        return $entity;
    }
}
