<?php
namespace App\Http\Specification\Response\Test;

use App\Http\Specification\ObjectType\TestChildObjectType;
use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class TestResponseSpecification extends ASpecification
{

    protected $specificationType = ASpecification::RESPONSE_SPECIFICATION;

    public function body()
    {
        return new ObjectType([
            'id' => new IntegerType(),
            'name' => new StringType(),
            'description' => new StringType(),
            'children' => new CollectionType(function(){
                return new TestChildObjectType();
            })
        ]);
    }
}