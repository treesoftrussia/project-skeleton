<?php

namespace App\DAL\Adapters;

use App\DAL\Model\ActivationCodeModel;
use App\Core\UserManagement\Entity\Support\ActivationCodeEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class ActivationCodeAdapter.
 */
class ActivationCodeAdapter extends AbstractAdapter
{
    /**
     * @param ActivationCodeModel $model
     *
     * @return ActivationCodeEntity
     */
    public function transform($model = null)
    {
        $entity = new ActivationCodeEntity();

        $entity
            ->setPhone($model->phone)
            ->setActivationCode($model->activation_code)
            ->setExpiresAt($model->expires_at)
            ->setRefreshAt($model->refresh_at)
        ;

        return $entity;
    }
}
