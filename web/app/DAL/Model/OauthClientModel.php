<?php

namespace App\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

class OauthClientModel extends Elegant
{
    protected $table = 'oauth_clients';

    public function scopes()
    {
        return $this->belongsToMany(ScopeModel::class, 'oauth_client_scopes', 'client_id', 'scope_id');
    }
}
