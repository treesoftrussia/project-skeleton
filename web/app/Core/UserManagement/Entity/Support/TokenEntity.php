<?php

namespace App\Core\UserManagement\Entity\Support;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

class TokenEntity
{
    /**
     * @var
     */
    private $accessToken;

    /**
     * @var
     */
    private $expiresIn;

    /**
     * @var
     */
    private $tokenType;

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     *
     * @return TokenEntity
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * @param $expiresIn
     *
     * @return $this
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = Cast::int($expiresIn);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @param mixed $tokenType
     *
     * @return TokenEntity
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;

        return $this;
    }
}
