<?php

namespace App\Core\Test\Services;

use App\Core\Test\Interfaces\TestRepositoryInterface;
use App\Core\Test\Entity\TestTEntity;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\ResourceNotFoundException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestService extends AbstractService
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        $testEntity = $repository->get($id);

        if (is_null($testEntity)) {
            throw new ResourceNotFoundException('Requested route was not found');
        }

        return $testEntity;
    }

    /**
     * @param TestTEntity $test
     *
     * @return mixed
     */
    public function create(TestTEntity $test)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        $testEntity = $repository->create($test);

        return $testEntity;
    }

    /**
     * @param $id
     * @param TestTEntity $test
     *
     * @return mixed
     */
    public function update($id, TestTEntity $test)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        $updated = $repository->update($id, $test);
        if (!$updated) {
            throw new RuntimeException('Resource Test Not Updated');
        }

        $testEntity = $repository->get($id);

        if (is_null($testEntity)) {
            throw new ResourceNotFoundException('Requested route was not found');
        }

        return $testEntity;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function exists($id)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        return $repository->exists($id);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        return $repository->delete($id);
    }
}
