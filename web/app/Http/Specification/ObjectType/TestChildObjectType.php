<?php
namespace App\Http\Specification\ObjectType;

use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class TestChildObjectType extends ObjectType
{
    public function structure()
    {
        return [
            'id' => new IntegerType(),
            'title' => new StringType()
        ];
    }
}