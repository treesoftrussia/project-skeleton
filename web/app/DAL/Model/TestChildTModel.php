<?php

namespace App\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildTModel extends Elegant
{
    protected $table = 'test_children_t';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
