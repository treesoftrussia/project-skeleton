<?php

use App\Tests\Endpoints\Support\ApplicationFactory;
use Mildberry\Kangaroo\QA\Endpoints\Application;

require __DIR__ . '/../../bootstrap/autoload.php';

Application::setConfig([
    'suitesNamespace' => 'App\Tests\Endpoints\Suites',
    'suitesName' => 'endpoints'
]);

Application::setFactory(new ApplicationFactory());
