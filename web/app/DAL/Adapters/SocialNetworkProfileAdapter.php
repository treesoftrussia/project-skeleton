<?php

namespace App\DAL\Adapters;

use App\Core\UserManagement\Entity\Users\SocialNetworkProfileEntity;
use App\DAL\Model\UserSocialNetworkModel;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class SocialNetworkProfileAdapter.
 */
class SocialNetworkProfileAdapter extends AbstractAdapter
{
    /**
     * @param UserSocialNetworkModel $model
     *
     * @return SocialNetworkProfileEntity
     */
    public function transform($model = null)
    {
        $entity = new SocialNetworkProfileEntity();
        $entity
            ->setId($model->id)
            ->setInternalId($model->user_internal_id)
            ->setType($model->type)
            ->setUserId($model->user_id)
        ;

        return $entity;
    }
}
