<?php

namespace App\Core\Test\Interfaces;

use App\Core\Test\Entity\TestTEntity;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface TestRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return TestTEntity|null
     */
    public function get($id);

    /**
     * @param TestTEntity $entity
     *
     * @return TestTEntity|null
     */
    public function create(TestTEntity $entity);

    /**
     * @param int    $id
     * @param TestTEntity $entity
     */
    public function update($id, TestTEntity $entity);

    /**
     * @param int $id
     *
     * @return bool
     */
    public function exists($id);

    /**
     * @param int $id
     */
    public function delete($id);
}
