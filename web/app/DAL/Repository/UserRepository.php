<?php

namespace App\DAL\Repository;

use App\Core\UserManagement\Entity\Users\SocialNetworkProfileEntity;
use App\Core\UserManagement\Entity\Users\UserEntity;
use App\Core\UserManagement\Interfaces\UserRepositoryInterface;
use App\DAL\Adapters\ActivationCodeAdapter;
use App\DAL\Adapters\RoleAdapter;
use App\DAL\Adapters\SocialNetworkProfileAdapter;
use App\DAL\Adapters\UserAdapter;
use App\DAL\FlattenAdapters\ActivationCodeFlattenAdapter;
use App\DAL\FlattenAdapters\UserFlattenAdapter;
use App\DAL\FlattenAdapters\UserSocialNetworkFlattenAdapter;
use App\DAL\Model\ActivationCodeModel;
use App\DAL\Model\RoleModel;
use App\DAL\Model\UserModel;
use App\Core\UserManagement\Entity\Support\ActivationCodeEntity as ActivationCodeEntity;
use App\DAL\Model\UserSocialNetworkModel;
use App\DAL\QueryObjects\AllUsersQueryObject;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @param $id
     *
     * @return \App\Core\UserManagement\Entity\Users\BaseUserEntity|null
     */
    public function getUser($id)
    {
        $userEntry = UserModel::where('id', $id)->with('roles')->first();
        $entity = new UserEntity();

        return is_null($userEntry) ? null : (new UserAdapter())->transform($userEntry, $entity);
    }

    public function delete($id)
    {
        UserModel::where('id', $id)->delete();
    }

    /**
     * @param $login
     *
     * @return UserEntity|null
     */
    public function getUserByCredentials($login)
    {
        $userEntry = UserModel::orWhere('email', $login)->orWhere('phone', $login)->orWhere('username', $login)->with('roles')->first();
        $entity = new UserEntity();

        return is_null($userEntry) ? null : (new UserAdapter())->transform($userEntry, $entity);
    }

    /**
     * @param UserEntity $user
     *
     * @return \App\Core\UserManagement\Entity\Users\BaseUserEntity
     */
    public function create(UserEntity $user)
    {
        $createdUser = UserModel::create((new UserFlattenAdapter())->transform($user));

        $userEntry = UserModel::find($createdUser->id);

        if ($userEntry) {
            $entity = new UserEntity();
            $userEntity = (new UserAdapter())->transform($userEntry, $entity);
            $userEntity->setPassword($user->getPassword());

            return $userEntity;
        } else {
            return false;
        }
    }

    /**
     * @param UserEntity $userEntity
     * @param array      $roleIds
     *
     * @return UserEntity|bool
     */
    public function setRoles(UserEntity $userEntity, array $roleIds)
    {
        $roleEntries = RoleModel::whereIn('machine_name', $roleIds)->get();
        if (!empty($roleEntries)) {
            $userEntry = UserModel::find($userEntity->getId());
            $userEntry->roles()->attach(array_pluck($roleEntries, 'id'));

            $userEntity->setRoles((new RoleAdapter())->transformCollection($roleEntries));

            return $userEntity;
        } else {
            return false;
        }
    }

    public function update($id, UserEntity $user)
    {
        UserModel::refresh($id, (new UserFlattenAdapter($user))->transform($user));

        return $this->getUser($id)->setPassword($user->getPassword());
    }

    public function checkOnExistUserByPhone($phone)
    {
        return UserModel::wherePhone($phone)->first() ? true : false;
    }

    public function checkOnExistUserByEmail($email)
    {
        return UserModel::whereEmail($email)->first() ? true : false;
    }

    public function getActivationCodeByPhone($phone)
    {
        $activationCodeEntry = ActivationCodeModel::wherePhone($phone)->first();

        return is_null($activationCodeEntry) ? $activationCodeEntry : (new ActivationCodeAdapter())->transform($activationCodeEntry);
    }

    public function checkOnActivityCode($phone)
    {
        $activationCodeEntry = ActivationCodeModel::where('phone', '=', $phone)
            ->where('expires_at', '>', time())
            ->first();

        return is_null($activationCodeEntry) ? false : true;
    }

    public function checkActivationCodeOnAvailabilityForRefresh($phone)
    {
        $activationCodeEntry = ActivationCodeModel::where('phone', '=', $phone)
            ->where('refresh_at', '<', time())
            ->first();

        return is_null($activationCodeEntry) ? false : true;
    }

    public function deleteActivationCodeByPhone($phone)
    {
        ActivationCodeModel::where('phone', '=', $phone)->delete();
    }

    public function checkOnActivityAndExistenceActivationCode($phone, $activationCode)
    {
        $activationCodeEntry = ActivationCodeModel::where('phone', '=', $phone)
            ->where('expires_at', '>', time())
            ->where('activation_code', '=', $activationCode)
            ->first();

        return is_null($activationCodeEntry) ? false : true;
    }

    public function createOrUpdateActivationCode(ActivationCodeEntity $activationCodeEntity)
    {
        $activationCodeExist = ActivationCodeModel::find($activationCodeEntity->getPhone());

        if (is_null($activationCodeExist)) {
            ActivationCodeModel::create((new ActivationCodeFlattenAdapter())->transform($activationCodeEntity));
        } else {
            ActivationCodeModel::refresh($activationCodeEntity->getPhone(), (new ActivationCodeFlattenAdapter())->transform($activationCodeEntity));
        }

        $activationCodeEntry = ActivationCodeModel::find($activationCodeEntity->getPhone());

        if ($activationCodeEntry) {
            $activationCodeEntity = (new ActivationCodeAdapter())->transform($activationCodeEntry);

            return $activationCodeEntity;
        }

        return false;
    }

    /**
     * @param $phone
     * @param $passwordHash
     *
     * @return mixed
     */
    public function updatePasswordByPhone($phone, $passwordHash)
    {
        return UserModel::wherePhone($phone)->update(['password' => $passwordHash]);
    }

    public function getAll(UserListOptions $options)
    {
        $query = (new AllUsersQueryObject())->build($options);

        if ($options->getPaginationOptions()) {
            $query = $query->forPage($options->getPaginationOptions()->getPageNum(), $options->getPaginationOptions()->getPerPage());
        }

        $usersEntries = $query->get();

        return (new UserAdapter())->transformCollection($usersEntries);
    }

    public function getTotal(UserListOptions $options)
    {
        $total = (new AllUsersQueryObject())->build($options)->count();

        return $total;
    }

    public function getUserByEmail($email)
    {
        $userEntry = UserModel::whereEmail($email)->first();

        $userEntity = new UserEntity();

        return is_null($userEntry) ? null : (new UserAdapter())->transform($userEntry, $userEntity);
    }

    public function getRoleIdByName($name)
    {
        return RoleModel::where('machine_name', '=', $name)->value('id');
    }

    public function assignRoleForUser($userId, $roleId)
    {
        $user = UserModel::find($userId);

        $exists = $user->roles->contains($roleId);
        if (!$exists) {
            $user->roles()->attach($roleId);
        }
    }

    /**
     * @param SocialNetworkProfileEntity $socialNetworkProfile
     *
     * @return SocialNetworkProfileEntity
     */
    public function createSocialNetworkProfile(SocialNetworkProfileEntity $socialNetworkProfile)
    {
        $createdSocialNetworkProfileModel = UserSocialNetworkModel::create((new UserSocialNetworkFlattenAdapter())->transform($socialNetworkProfile));

        return (new SocialNetworkProfileAdapter())->transform(UserSocialNetworkModel::find($createdSocialNetworkProfileModel->id));
    }

    /**
     * @param UserEntity                 $user
     * @param SocialNetworkProfileEntity $socialNetworkProfile
     * @param UserOptions|null           $options
     */
    public function createOrUpdateIfExistsSocialNetworkEntry(UserEntity $user, SocialNetworkProfileEntity $socialNetworkProfile, UserOptions $options = null)
    {
        $context = $this;

        return DB::transaction(function () use ($context, $user, $socialNetworkProfile, $options) {
            $socialNetworkModel = UserSocialNetworkModel::whereType($socialNetworkProfile->getType())->whereInternalUserId($socialNetworkProfile->getInternalId())->sharedLock()->first();

            if (!$socialNetworkModel) {
                $user = $context->create($user);
                if ($options->getWithSocialNetworkEntry()) {
                    $socialNetworkProfile->setUserId($user->getId());
                    $context->createSocialNetworkProfile($socialNetworkProfile);
                }
            } else {
                $user = $context->update($socialNetworkModel->user_id, $user);
            }

            return $user;
        });
    }
}
