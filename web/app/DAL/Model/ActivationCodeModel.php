<?php

namespace App\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

class ActivationCodeModel extends Elegant
{
    protected $table = 'activation_codes';

    protected $primaryKey = 'phone';

    public $incrementing = false;

    public $timestamps = false;
}
