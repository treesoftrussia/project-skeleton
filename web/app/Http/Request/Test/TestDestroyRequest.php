<?php

namespace App\Http\Requests\Test;

use Mildberry\Kangaroo\Libraries\Requests\APIRequest;


/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestDestroyRequest extends APIRequest
{
}
