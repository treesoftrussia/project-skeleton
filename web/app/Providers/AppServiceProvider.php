<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Mildberry\Kangaroo\Libraries\Adapter\SharedModifiers as AdapterModifiers;
use Mildberry\Kangaroo\Libraries\Adapter\SharedSanitizers as AdapterSanitizers;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Register shared modifiers  for all adapters
         */
        AbstractAdapter::setSharedModifiersProvider(AbstractAdapter::M_MODIFIER, new AdapterModifiers());

        /**
         * Register shared sanitizers for all adapters
         */
        AbstractAdapter::setSharedModifiersProvider(AbstractAdapter::M_SANITIZER, new AdapterSanitizers());

    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
